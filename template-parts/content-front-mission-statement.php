<?php
/**
 * The template used for displaying testimonials on the Front Page.
 *
 * @package Canape
 */
?>


	<div id="front-page-mission-statement" class="front-mission">
		<div class="inner">
      <?php the_field('mission_statement'); ?>
		</div>
	</div><!-- .front-testimonials -->
