<?php
/**
 * The template used for displaying featured food menu categories on the Front Page.
 *
 * @package Canape
 */

if( have_rows('repeater') ): ?>
	

	<div id="front-page-menu" class="front-featured-menu-items menu">
		<div class="grid-row">
			<?php while( have_rows('repeater') ): the_row(); 

    		// vars
    		$image = get_sub_field('image');
    		$text = get_sub_field('text');
    		$link = get_sub_field('link');
    
      ?>
      
      <div class="item">
        <a href="<?php echo esc_url($link); ?>" class="menu-section-thumbnail">
          <img src="<?php echo $image['sizes']['canape-special-area']; ?>">
						<div class="overlay">
							<div class="overlay-inner">
								<h2><?php echo esc_html( the_sub_field('title') ); ?></h2>
								<p class="description"><?php echo esc_html( the_sub_field('text') ); ?></p>
							</div>
						</div>

					</a>
				</div>
			<?php endwhile; ?>
		</div>
	</div>

<?php endif; ?>
