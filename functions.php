<?php

/**
 * sjumission functions and definitions
 *
 * @package sjumission
 */

/**
 * Activates default theme features
 *
 * @since 1.0
 */
function sjumission_theme_setup(){

	  add_image_size( 'canape-special-area', 345, 480, true );

	
}
add_action( 'after_setup_theme', 'sjumission_theme_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function sjumission_widgets_init() {
	register_sidebar( array(
		'name'			=> __( 'Sidebar', 'sjumission' ),
		'id'			=> 'sidebar-1',
		'before_widget'	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h1 class="widget-title">',
		'after_title'	=> '</h1>',
	) );
}
// Uncomment the to add a new widgetized area
// add_action( 'widgets_init', 'sjumission_widgets_init' );

/**
 * Register our scripts (js/css)
 *
 * @since 1.0
 */
function sjumission_enqueue_scripts(){

	// Uncomment the line below and add a
	// scripts.js file to your child theme
	// to add custom javascript to your child theme
	// wp_enqueue_script( 'sjumission-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array(), '20140101', true );
	
}
// Uncomment this to add a additional scripts
//add_action( 'wp_enqueue_scripts', 'sjumission_enqueue_scripts' );


function register_reflection_cpt() {
	if ( post_type_exists( 'reflection' ) ) {
		return;
	}

	register_post_type( 'reflection' , array(
		'description' => __( 'Reflections', 'jetpack' ),
		'labels' => array(
			'name'                  => 'Reflections',
			'singular_name'         => 'Reflection',
			'menu_name'             => 'Reflections',
			'all_items'             => 'All Reflections',              
			'add_new'               => 'Add New',                       
			'add_new_item'          => 'Add New Reflection',           
			'edit_item'             => 'Edit Reflection',              
			'new_item'              => 'New Reflection',               
			'view_item'             => 'View Reflection',              
			'search_items'          => 'Search Reflections',           
			'not_found'             => 'No Reflections found',         
			'not_found_in_trash'    => 'No Reflections found in Trash',
			'filter_items_list'     => 'Filter Reflections list',      
			'items_list_navigation' => 'Reflection list navigation',   
			'items_list'            => 'Reflections list',             
		),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'page-attributes',
		),
		'rewrite' => array(
			'slug'       => 'reflection',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => true,
		),
		'public'          => true,
		'show_ui'         => true,
		'menu_position'   => 20, // below Pages
		'menu_icon'       => 'dashicons-testimonial',
		'capability_type' => 'page',
		'map_meta_cap'    => true,
		'has_archive'     => true,
		'query_var'       => 'reflection',
	) );
}


add_action( 'init', 'register_reflection_cpt', 0 );